import React, { Component } from "react";
import Board from "./Boards";
import Sidebar from "./Sidebar";
import Topbar from "./Topbar";

class MainLayout extends Component {
  render() {
    return (
      <div>
        <Sidebar />
        <div className="main p-4">
          <Topbar />
          <Board />
        </div>
      </div>
    );
  }
}

export default MainLayout;
