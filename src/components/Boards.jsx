import React, { Component } from "react";
import { DragDropContext } from "react-beautiful-dnd";

import BoardData from "../data/board.json";
import IssuesData from "../data/issues.json";

import BoardItem from "./BoardItem";

class Boards extends Component {
  constructor() {
    super();
    this.state = {
      boardData: BoardData,
      issuesData: IssuesData,
    };
  }
  onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    const start = this.state.boardData[source.droppableId];
    const finish = this.state.boardData[destination.droppableId];

    if (start === finish) {
      const newIssuesId = Array.from(start.issuesId);
      newIssuesId.splice(source.index, 1);
      newIssuesId.splice(destination.index, 0, draggableId);

      const newBoard = {
        ...start,
        issuesId: newIssuesId,
      };

      const newBoardData = this.state.boardData;
      newBoardData[newBoard.id] = newBoard;
      console.log(newBoardData);

      const newState = {
        ...this.state,
        boardData: newBoardData,
      };
      this.setState(newState);
      return;
    }

    // moving to another board
    const startIssuesId = Array.from(start.issuesId);
    startIssuesId.splice(source.index, 1);
    const newStart = {
      ...start,
      issuesId: startIssuesId,
    };

    const finishIssuesId = Array.from(finish.issuesId);
    finishIssuesId.splice(destination.index, 0, draggableId);
    const newFinish = {
      ...finish,
      issuesId: finishIssuesId,
    };

    const newBoardData = this.state.boardData;
    newBoardData[newStart.id] = newStart;
    newBoardData[newFinish.id] = newFinish;

    const newState = {
      ...this.state,
      boardData: newBoardData,
    };
    this.setState(newState);
  };

  render() {
    console.log(this.state.boardData);
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <div className="row mt-5 px-3 mr-5">
          {this.state.boardData.map((boards, index) => {
            const board = this.state.boardData[boards.id];
            const issues = board.issuesId.map(
              (id) => this.state.issuesData[id]
            );
            return (
              <BoardItem
                key={board.id}
                title={board.title}
                id={board.id}
                issues={issues}
              />
            );
          })}
        </div>
      </DragDropContext>
    );
  }
}
export default Boards;
