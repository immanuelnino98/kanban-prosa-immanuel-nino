import React, { Component } from "react";
import { Draggable } from "react-beautiful-dnd";
import classnames from "classnames";
import moment from "moment";

class CardIssue extends Component {
  renderTag(tags) {
    switch (tags) {
      case "Designer":
        return "tag-blue";
        break;
      case "Research":
        return "tag-yellow";
        break;
      case "Backend":
        return "tag-red";
        break;
      default:
        return "tag-blue";
        break;
    }
  }

  render() {
    const { issuesData } = this.props;

    let start = moment(issuesData.start_date).format("YYYY-MM-DD");
    start = moment(start, "YYYY-MM-DD");
    let end = moment(issuesData.end_date).format("YYYY-MM-DD");
    end = moment(end, "YYYY-MM-DD");

    const remainingDays = moment.duration(end.diff(start)).asDays();

    return (
      <Draggable draggableId={issuesData.issue_id} index={this.props.index}>
        {(provided) => (
          <div
            className="card d-block border-0 bg-card mx-0 px-3 pt-3 pb-2 mb-2"
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            <h5>{issuesData.title}</h5>
            <div>
              <div className="d-inline align-middle ml-0">
                <h5 className="d-inline-block initial-tag rounded-circle border-white p-1 px-2 mb-0">
                  {issuesData.assignee.charAt(0)}
                </h5>
                <h6
                  className={classnames(
                    "d-inline rounded-pill font-weight-bold px-2 mb-0",
                    this.renderTag(issuesData.tags)
                  )}
                >
                  {issuesData.tags}
                </h6>
              </div>
              <div className="d-inline align-middle float-right text-secondary my-1">
                <h6>{remainingDays} days</h6>
              </div>
            </div>
          </div>
        )}
      </Draggable>
    );
  }
}
export default CardIssue;
