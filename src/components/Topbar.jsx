import React, { Component } from "react";

class Topbar extends Component {
  render() {
    return (
      <div className="row justify-content-between align-items-center">
        <div className="col">
          <div className="d-inline-block">
            <img
              src="https://media-exp1.licdn.com/dms/image/C510BAQFACMedGj9Sog/company-logo_200_200/0/1563345657159?e=2159024400&v=beta&t=oztOHm4RGJgnKJNPc-QDOQC_FFm-TrSqt74azHvUp8s"
              alt="logo"
              width="50px"
            />
          </div>
          <div className="d-inline align-middle ml-0">
            <h2 className="d-inline-block font-weight-bold mb-0">
              Kanban Prosa
            </h2>
          </div>
          <div className="d-inline align-middle ml-5">
            <h5 className="d-inline text-secondary bg-gray rounded-circle p-1 px-2 mb-0">
              <i className="fas fa-ellipsis-h"></i>
            </h5>
          </div>
        </div>
        <div className="col text-right mr-4">
          <img
            className="rounded-circle profile-img mr-0"
            width="30px"
            height="30px"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6AbOxsAx72Yoema07B6ekBkNSbzqjubmqVw&usqp=CAU"
            alt="photo"
          />
          <img
            className="rounded-circle profile-img mr-0"
            width="30px"
            height="30px"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRb5SaDNJsmXZS3qzGWPjdwQVc9FDOTRfuxUQ&usqp=CAU"
            alt="photo"
          />
          <img
            className="rounded-circle profile-img mr-0"
            width="30px"
            height="30px"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw02wZImLc5FlzdhiCLBEud4gRku20q0eQTA&usqp=CAU"
            alt="photo"
          />
          <h5 className="d-inline bg-gray rounded-pill py-1 px-3 ml-2">
            70 Members
          </h5>
        </div>
      </div>
    );
  }
}

export default Topbar;
