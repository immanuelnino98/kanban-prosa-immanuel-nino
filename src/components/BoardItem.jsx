import React, { Component } from "react";
import { Droppable } from "react-beautiful-dnd";
import CardIssue from "./CardIssue";
import Modal from "./Modal";

class BoardItem extends Component {
  render() {
    const { id, title, issues } = this.props;
    return (
      <div className="col-4">
        <div className="card shadow-sm">
          <div className="card-body">
            <div>
              <div className="d-inline align-middle ml-0">
                <h4 className="d-inline-block font-weight-bold mb-0">
                  {title}
                </h4>
              </div>
              <div className="d-inline align-middle float-right my-1">
                <h6
                  className="d-inline text-white bg-button rounded-pill font-weight-bold p-2 px-3 mb-0"
                  type="button"
                  data-toggle="modal"
                  data-target="#Modal"
                >
                  + Add Task
                </h6>
              </div>
            </div>
            <Droppable droppableId={id}>
              {(provided) => (
                <div
                  className="mt-4"
                  style={{ minHeight: "20px" }}
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                >
                  {issues.map((issue, index) => {
                    return (
                      <CardIssue
                        key={issue.issue_id}
                        issuesData={issue}
                        index={index}
                      />
                    );
                  })}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        </div>
        <Modal />
      </div>
    );
  }
}
export default BoardItem;
