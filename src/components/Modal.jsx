import React, { Component } from "react";

class Modal extends Component {
  render() {
    return (
      <div
        class="modal fade"
        id="Modal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="ModalLabel"
        aria-hidden="true"
      >
        <div
          class="modal-dialog modal-dialog-centered"
          role="document"
          style={{ maxWidth: "300px" }}
        >
          <div class="modal-content p-4">
            <h4 className="mb-4">New Tasks</h4>
            <form>
              <div class="form-group">
                <label>Title</label>
                <input
                  type="text"
                  class="form-control form-control-sm"
                  placeholder="Enter title"
                />
              </div>
              <div class="form-group">
                <label>Tags</label>
                <input
                  type="text"
                  class="form-control form-control-sm"
                  placeholder="Enter tags"
                />
              </div>
              <div class="form-group">
                <label>Assignee</label>
                <input
                  type="text"
                  class="form-control form-control-sm"
                  placeholder="Enter assignee"
                />
              </div>
              <div class="form-group">
                <label>Start Date</label>
                <input type="date" class="form-control form-control-sm" />
              </div>
              <div class="form-group">
                <label>End Date</label>
                <input type="date" class="form-control form-control-sm" />
              </div>
              <div className="text-center">
                <button type="submit" class="btn bg-button text-white">
                  <h5 className="mb-0">Submit</h5>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default Modal;
