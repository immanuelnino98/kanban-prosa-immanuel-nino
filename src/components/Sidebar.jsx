import React, { Component } from "react";

class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar text-center d-inline-block p-3 ">
        <div className="mb-5">
          <form className="d-block">
            <input
              className="search-input text-white border-0 py-1"
              type="text"
              placeholder="Search.."
              name="search"
            />
            <button
              className="search-input-button text-white border-0 px-2 py-1"
              type="submit"
            >
              <i className="fa fa-search"></i>
            </button>
          </form>
        </div>
        <div className="row align-items-center justify-content-md-center text-left text-white pl-2 pt-3">
          <div className="col-3 px-0 text-center">
            <img
              className="rounded-circle profile-img"
              width="50px"
              height="50px"
              src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NXx8cG9ydHJhaXR8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80"
              alt="photo"
            />
          </div>
          <div className="col-9 text-left pl-2 pr-0">
            <div>
              <h5 className="mb-1">Emilee Simchenko</h5>
              <h6 className="text-black">Product Owner</h6>
            </div>
          </div>
          <div className="col-6 px-0 text-white pl-2 mt-2">
            <h4 className="mb-0">372</h4>
            <p>Completed Tasks</p>
          </div>
          <div className="col-6 text-white px-2">
            <h4 className="mb-0">11</h4>
            <p>Open Tasks</p>
          </div>
        </div>
        <div className="text-left text-white mt-4">
          <h6 className="text-black mb-3">MENU</h6>
          <h5 className="mb-3">Home</h5>
          <h5 className="mb-3">My Tasks</h5>
          <h5 className="mb-3">Notification</h5>
        </div>
        <div className="text-left text-white mt-4 pt-4">
          <h6 className="text-black mb-3">TEAMS</h6>
          <div className="overflow-hidden mb-3">
            <h5 className="float-left my-1">Researchers</h5>
            <div className="float-right">
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                alt="photo"
              />
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://i.ytimg.com/vi/Y44IDbQMKMw/maxresdefault.jpg"
                alt="photo"
              />
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8cG9ydHJhaXR8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80"
                alt="photo"
              />
            </div>
          </div>
          <div className="overflow-hidden mb-3">
            <h5 className="float-left my-1">FE/FB Team</h5>
            <div className="float-right">
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://img.freepik.com/free-photo/portrait-white-man-isolated_53876-40306.jpg?size=626&ext=jpg"
                alt="photo"
              />
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRM70bc_MwqsUOdjIUjSbP1Xmh0Mqf0yS5xbw&usqp=CAU"
                alt="photo"
              />
            </div>
          </div>
          <div className="overflow-hidden mb-3">
            <h5 className="float-left my-1">PM Team</h5>
            <div className="float-right">
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-kxJtBZKbGZ2gnCUzqL3UkPOeU3nLpVkkOg&usqp=CAU"
                alt="photo"
              />
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6AbOxsAx72Yoema07B6ekBkNSbzqjubmqVw&usqp=CAU"
                alt="photo"
              />
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRb5SaDNJsmXZS3qzGWPjdwQVc9FDOTRfuxUQ&usqp=CAU"
                alt="photo"
              />
              <img
                className="rounded-circle profile-img mr-0"
                width="20px"
                height="20px"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw02wZImLc5FlzdhiCLBEud4gRku20q0eQTA&usqp=CAU"
                alt="photo"
              />
            </div>
          </div>
          <h6 className="text-white-50">+ Add a Team</h6>
        </div>
      </div>
    );
  }
}

export default Sidebar;
